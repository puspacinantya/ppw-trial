from django.shortcuts import render

# Create your views here.
def homepage(request):
    return render(request, 'homepage.html')

def newproject(request):
    return render(request, 'newproject.html')

def mywritings(request):
    return render(request, 'mywritings.html')

def testimonials(request):
    return render(request, 'testimonials.html')

def contact(request):
    return render(request, 'contact.html')

def education(request):
    return render(request, 'education.html')

from django.conf.urls import url
from .views import homepage
from .views import newproject
from .views import mywritings
from .views import testimonials
from .views import contact
from .views import education
from .views import guestbook
#url for app
urlpatterns = [
    url(r'^homepage', homepage, name='homepage'),
    url(r'^myproject/', newproject, name='newproject'),
    url(r'^mywritings/', mywritings, name='mywritings'),
    url(r'^testimonials/', testimonials, name='testimonials'),
    url(r'^contact/', contact, name='contact'),
    url(r'^education/', education, name='education'),
    url(r'^guestbook/', guestbook, name='guestbook'),
]
from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Puspacinantya' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 4, 8) #TODO Implement this, format (Year, Month, Date)
npm = 1706043821 # TODO Implement this
hobby = 'sleep and eat'
deskripsi_diri = 'semangat, lincah, gembira'

mhs_name_kanan = 'Salsabila Maurizka'
npm_kanan = 1706043986
birth_date_kanan= date(2000, 4, 14)
hobby_kanan = 'fencing'
deskripsi_diri_kanan = 'menarik, fast response, suka ngantuk'

mhs_name_kiri = 'Tembok lab 1103'
npm_kiri = 'saya tembok, gapunya npm'
birth_date_kiri = 'sejak gedung a ada, saya ada'
hobby_kiri = 'gaada'
deskripsi_diri_kiri = 'gapunya deskripsi diri, dia tembok'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'hobby' : hobby, 'deskripsiku' : deskripsi_diri, 
    'namakanan' : mhs_name_kanan, 'npmkanan' : npm_kanan, 'umurkanan': calculate_age(birth_date_kanan.year), 'hobikanan': hobby_kanan, 'deskripsikanan' : deskripsi_diri_kanan,
    'namakiri': mhs_name_kiri, 'npmkiri': npm_kiri, 'umurkiri': birth_date_kiri, 'hobikiri': hobby_kiri, 'deskripsikiri': deskripsi_diri_kiri}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
